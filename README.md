Ogg.php
=======

What is Ogg.php ?
-----------------

Ogg.php is an event based parser for Ogg streams, written in... PHP.

Introduction
------------

This library is slightly inspired by [File_Ogg](http://pear.php.net/package/File_Ogg) from Tim Starling and
Jan Gerber but aims a different design. In theory, an Ogg stream may not have
end. So we need an Ogg parser which doesn't store streams data. That's what
event-based parsing is designed for. Data storing strategy is the final user's
business whereas the parser only processes a left to right reading and triggers
an event each time it reads something interesting.

Installation
------------

```shell
composer require coste/ogg
```

Examples
--------

### Counting the number of packets

```php
<?php
require_once 'vendor/autoload.php';

use Coste\Ogg\Parser as Ogg;

$fp = fopen('song.ogg', 'r');
$count = 0;

(new Ogg($fp))
  ->on('ogg:packet:start', function() use (&$count) {
    $count += 1;
  })
  ->run();

echo "There is {$count} packets in this stream" . PHP_EOL;
```

### Displaying the list of streams

```php
<?php
require_once 'vendor/autoload.php';

use Coste\Ogg\Parser as Ogg;

$fp = fopen('song.ogg', 'r');
(new Ogg($fp))
  ->on('ogg:stream:start', function($serial) {
    echo "Stream ID: {$serial}" . PHP_EOL;
  })
  ->run();
```

### Extracting a raw stream out of Ogg container

Given we want to extract a stream and it's serial ID is `851056896` :

```php
<?php
require_once 'vendor/autoload.php';

use Coste\Ogg\Parser as Ogg;

$input  = fopen("song.ogg", "r");
$output = fopen("song.raw", "w");

(new Ogg($fp))
  ->on('ogg:packet:end', function($serial, $content) use ($output) {
    if ($serial == 851056896) {
       fwrite($output, $content);
    }
  })
  ->run();

fclose($input);
fclose($output);
```

List of events (work in progress)
---------------------------------

| Event name       | Arguments |
|------------------|-----------|
| ogg:packet:start | ?         |
| ogg:packet:end   | ?         |
| ogg:header:start | ?         |
| ogg:header:end   | ?         |
| ogg:stream:start | ?         |
| ogg:stream:end   | ?         |


Contributing
------------

If you have some ideas of improvement or if you are experimenting some bugs,
just let me know in the bugtracker or by sending an email to charles-edouard@coste.dev.
To avoid intellectual property problems and to be sure I can keep this code
under a Free (as Freedom) licence, code contribution will not be accepted until
I find an easy process for contributor licence agreement.

Licence
-------

[![AGPL](http://www.gnu.org/graphics/agplv3-155x51.png)](http://www.gnu.org/licenses/agpl-3.0.html)

Copyright (C) 2016-2021 Charles-Édouard Coste

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

[![PLAY OGG](http://static.fsf.org/playogg/play_ogg_medium.png)](http://playogg.org)
