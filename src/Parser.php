<?php

namespace Coste\Ogg;

/**
 * @author Charles-Édouard Coste <charles-edouard@coste.dev>
 */
class Parser
{
    const CAPTURE_PATTERN = "\x4f\x67\x67\x53"; // ASCII encoded "OggS" string
    const BOS = 2;
    const EOS = 4;

    /**
     * This is a pointer to a stream
     */
    private $fp;

    /**
     * This is a list of callbacks to call when events occur
     */
    private $callbacks;

    public function __construct($fp)
    {
        $this->fp = &$fp;
    }

    public function on($event, $callback)
    {
        $this->callbacks[$event][] = $callback;
        return $this;
    }

    private function emit($event, $arguments = [])
    {
        if (empty($this->callbacks[$event])) {
            return;
        }

        $arguments = array_merge((array)$event, $arguments);

        foreach ($this->callbacks[$event] as $callback) {
            $callback(...$arguments);
        }
    }

    public function run()
    {
        $pack_format = [
            'stream_structure_version' => 'C',
            'header_type_flag'         => 'C',
            'granule_position'         => 'A8',
            'bitstream_serial_number'  => 'V',
            'page_sequence_number'     => 'V',
            'CRC_checksum'             => 'V',
            'number_page_segments'     => 'C'
        ];

        $pack_format = implode(
            '/',
            array_map(
                function ($v, $k) {
                    return $v . $k;
                },
                $pack_format,
                array_keys($pack_format)
            )
        );

        while ($capture_pattern = fread($this->fp, 4)) {
            if ($capture_pattern !== self::CAPTURE_PATTERN) {
                throw new Exception("Bad capture pattern");
            }

            $this->emit('ogg:packet:start');

            $headers = unpack(
                $pack_format,
                fread($this->fp, 23)
            );

            if ($headers['stream_structure_version'] != 0x00) {
                throw new Exception("Bad stream version");
            }

            for ($i = 0; $i < $headers['number_page_segments']; ++$i) {
                $segment_length = unpack(
                    "Cdata",
                    fread($this->fp, 1)
                );

                $headers['segment_table'][] = $segment_length['data'];
            }

            $headers['CRC_checksum'] = sprintf("%u", $headers['CRC_checksum']);

            $this->emit('ogg:header:start', [$headers]);

            $serial = $headers['bitstream_serial_number'];

            if ($headers['header_type_flag'] & self::BOS) {
                $this->emit('ogg:stream:start', [$serial]);
            } elseif ($headers['header_type_flag'] & self::EOS) {
                $this->emit('ogg:stream:end', [$serial]);
            } else {
                $this->emit('ogg:stream:continue', [$serial]);
            }

            foreach ($headers['segment_table'] as $segment_size) {
                $this->emit('ogg:segment:start', [$segment_size]);
                if ($segment_size > 0) {
                    $content = fread($this->fp, $segment_size);
                }
                $this->emit('ogg:segment:end', [$content]);
            }
            $this->emit('ogg:packet:end');
        }
    }
}
